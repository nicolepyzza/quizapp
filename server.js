const express = require("express");
const mongoose = require("mongoose");
const cors = require('cors');
const bodyParser = require("body-parser");
const app = express();
const passport = require('passport');

app.use(cors());

const users = require('./routes/api/users');

app.use(express.urlencoded({extended: false}));
app.use(express.json());

const db = require("./config/keys").mongoURI;

mongoose.connect(db,
    { useNewUrlParser: true }
  ).then(() => console.log("MongoDB successfully connected"))
  .catch(err => console.log(err));

app.use(passport.initialize());

require('./config/passport')(passport);

app.use('/api/users', users);

const port = process.env.PORT || 4000;
app.listen(port, () => console.log(`Server running on port ${port}!`));